package homework.chegg.com.chegghomework.db

import androidx.room.TypeConverter
import com.google.gson.Gson

import com.google.gson.reflect.TypeToken
import homework.chegg.com.chegghomework.model.modela.Story
import homework.chegg.com.chegghomework.model.modelb.Metadata
import homework.chegg.com.chegghomework.model.modelc.ModelCItem
import java.lang.reflect.Type


/**
 * Created by alexkorolov on 07/05/2020.
 */
class DBConverters {


    @TypeConverter
    fun stringToStoryList(data: String?): List<Story>? {
        val gson = Gson()
        if (data == null) {
            return null
        }
        val listType: Type = object : TypeToken<List<Story>?>() {}.type
        return gson.fromJson<List<Story>>(data, listType)
    }

    @TypeConverter
    fun storyListToString(navList: List<Story>?): String? {
        val gson = Gson()
        return gson.toJson(navList)
    }

    @TypeConverter
    fun stringToMetadata(data: String?): Metadata? {
        val gson = Gson()
        if (data == null) {
            return null
        }
        val listType: Type = object : TypeToken<Metadata?>() {}.type
        return gson.fromJson<Metadata>(data, listType)
    }

    @TypeConverter
    fun metadataToString(metadata: Metadata?): String? {
        val gson = Gson()
        return gson.toJson(metadata)
    }

    @TypeConverter
    fun stringToItemCList(data: String?): ArrayList<ModelCItem>? {
        val gson = Gson()
        if (data == null) {
            return null
        }
        val listType: Type = object : TypeToken<List<ModelCItem>?>() {}.type
        return gson.fromJson<ArrayList<ModelCItem>>(data, listType)
    }

    @TypeConverter
    fun itemCListToString(navList: ArrayList<ModelCItem>?): String? {
        val gson = Gson()
        return gson.toJson(navList)
    }

}