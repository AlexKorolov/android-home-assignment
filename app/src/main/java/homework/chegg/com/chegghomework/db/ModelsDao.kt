package homework.chegg.com.chegghomework.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import homework.chegg.com.chegghomework.model.BaseModel
import homework.chegg.com.chegghomework.model.modela.ModelA
import homework.chegg.com.chegghomework.model.modelb.ModelB
import homework.chegg.com.chegghomework.model.modelc.ModelC

@Dao
interface ModelsDao {

    @Query("SELECT * FROM ModelA")
    fun getModelA(): ModelA

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertModelA(modelA: ModelA)

    @Query("SELECT * FROM ModelB")
    fun getModelB(): ModelB

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertModelB(modelB: ModelB)

    @Query("SELECT * FROM ModelC")
    fun getModelC(): ModelC

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertModelC(modelC: ModelC)

    @Query("DELETE FROM ModelA")
    fun clearA()

    @Query("DELETE FROM ModelB")
    fun clearB()

    @Query("DELETE FROM ModelC")
    fun clearC()

    fun clearAll() {
        clearA()
        clearB()
        clearC()
    }

    fun insertModel(model: BaseModel) {
        when (model) {
            is ModelA -> insertModelA(model)
            is ModelB -> insertModelB(model)
            is ModelC -> insertModelC(model)
        }
    }
}