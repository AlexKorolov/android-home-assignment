package homework.chegg.com.chegghomework.model

import android.util.Log
import androidx.room.Ignore
import androidx.room.PrimaryKey
import homework.chegg.com.chegghomework.common.extensions.toCheggItem

open class BaseModel : CheggItemConverter {
    @PrimaryKey
    var key: Long = 0
    var expiration: Long = 0

    @Ignore
    open var CACHE_TIME: Long = 0

    fun updateExpirationTime() {
        Log.e("ALEX", "updateExpirationTime $CACHE_TIME")
        expiration = System.currentTimeMillis() + CACHE_TIME
    }

    fun isExpired(): Boolean {
        return System.currentTimeMillis() - expiration > CACHE_TIME
    }

    override fun prepareCheggItemList(): List<CheggItem> {
        return emptyList()
    }
}