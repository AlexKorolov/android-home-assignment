package homework.chegg.com.chegghomework.common.extensions

import homework.chegg.com.chegghomework.model.CheggItem
import homework.chegg.com.chegghomework.model.modela.Story
import homework.chegg.com.chegghomework.model.modelb.Innerdata
import homework.chegg.com.chegghomework.model.modelc.ModelCItem

fun Story.toCheggItem() = CheggItem(
    imageUrl = imageUrl,
    subtitle = subtitle,
    title = title
)

fun Innerdata.toCheggItem() = CheggItem(
    imageUrl = picture,
    subtitle = articlewrapper.description,
    title = articlewrapper.header
)

fun ModelCItem.toCheggItem() = CheggItem(
    imageUrl = image,
    subtitle = subLine1 + subline2,
    title = topLine
)