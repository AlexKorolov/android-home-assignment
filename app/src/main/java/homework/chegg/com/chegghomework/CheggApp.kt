package homework.chegg.com.chegghomework

import android.app.Application
import homework.chegg.com.chegghomework.di.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CheggApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@CheggApp)
            modules(mainModule)
        }
    }
}