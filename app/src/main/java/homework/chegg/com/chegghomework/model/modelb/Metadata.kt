package homework.chegg.com.chegghomework.model.modelb

data class Metadata(
    val innerdata: List<Innerdata>,
    val `this`: String
)