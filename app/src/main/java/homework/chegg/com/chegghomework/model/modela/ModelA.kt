package homework.chegg.com.chegghomework.model.modela

import androidx.room.Entity
import homework.chegg.com.chegghomework.common.extensions.toCheggItem
import homework.chegg.com.chegghomework.model.BaseModel
import homework.chegg.com.chegghomework.model.CheggItem
import homework.chegg.com.chegghomework.model.CheggItemConverter

@Entity
data class ModelA(
    val stories: List<Story>
) : BaseModel() {

    override fun prepareCheggItemList(): List<CheggItem> {
        return stories.map { data -> data.toCheggItem() }
    }

    override var CACHE_TIME: Long
        get() = 5 * 60 * 1000
        set(value) {}
}