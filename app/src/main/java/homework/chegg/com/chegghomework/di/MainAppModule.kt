package homework.chegg.com.chegghomework.di

import android.content.Context
import androidx.room.Room
import homework.chegg.com.chegghomework.common.Consts
import homework.chegg.com.chegghomework.db.AppDatabase
import homework.chegg.com.chegghomework.network.CheggApi
import homework.chegg.com.chegghomework.repositories.CheggRepository
import homework.chegg.com.chegghomework.ui.MainViewModel
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

val mainModule = module {

    single { createWebService() }
    single { getDatabase(get()) }

    factory { CheggRepository(get(), get()) }

    viewModel { MainViewModel(get()) }
}

fun createWebService(): CheggApi {

    val interceptor = HttpLoggingInterceptor()
    interceptor.level = HttpLoggingInterceptor.Level.BODY

    val okkHttpclient = buildClient().let {
        it.addInterceptor(interceptor)
        it.build()
    }
    return Retrofit.Builder()
        .client(okkHttpclient)
        .baseUrl(Consts.BASE_URL)
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(CheggApi::class.java)

}

fun buildClient(): OkHttpClient.Builder {
    return OkHttpClient.Builder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .writeTimeout(60, TimeUnit.SECONDS)
}

fun getDatabase(context: Context): AppDatabase {
    synchronized(context) {
        return Room.databaseBuilder(
            context.applicationContext,
            AppDatabase::class.java,
            AppDatabase.DB_NAME
        ).fallbackToDestructiveMigration().build()
    }
}