package homework.chegg.com.chegghomework.common

import java.util.concurrent.TimeUnit

object Consts {
    const val BASE_URL = "https://chegg-mobile-promotions.cheggcdn.com/"

    const val DATA_SOURCE_A_URL = "ios/home-assignments/source_a.json"
    const val DATA_SOURCE_B_URL = "ios/home-assignments/source_b.json"
    const val DATA_SOURCE_C_URL = "ios/home-assignments/source_c.json"

    const val NO_ANIMATION_DELAY = 0L
    const val TWO_SECONDS_ANIMATION_DELAY = 2_000L
}