package homework.chegg.com.chegghomework.model.modelc

data class ModelCItem(
    val image: String,
    val subLine1: String,
    val subline2: String,
    val topLine: String
)