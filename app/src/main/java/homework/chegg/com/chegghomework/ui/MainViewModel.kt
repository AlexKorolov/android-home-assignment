package homework.chegg.com.chegghomework.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import homework.chegg.com.chegghomework.common.ActionState
import homework.chegg.com.chegghomework.common.CheggItemsState
import homework.chegg.com.chegghomework.common.Consts
import homework.chegg.com.chegghomework.repositories.CheggRepository
import kotlinx.coroutines.*

class MainViewModel(private val cheggRepository: CheggRepository) : ViewModel() {

    private val _itemsData = MutableLiveData<CheggItemsState>()
    val itemsData: LiveData<CheggItemsState>
        get() = _itemsData

    private var requestDelay = Consts.TWO_SECONDS_ANIMATION_DELAY

    fun getAllSourceData() {
        viewModelScope.launch(exceptionHandler) {
            withContext(Dispatchers.IO) {

                _itemsData.postValue(ActionState.loading())

                val modelA = async { cheggRepository.getDataSourceA() }
                val modelB = async { cheggRepository.getDataSourceB() }
                val modelC = async { cheggRepository.getDataSourceC() }

                modelA.await()
                modelB.await()
                modelC.await()

                delay(requestDelay) // For animation purpose only

                val allData = modelA.getCompleted() + modelB.getCompleted() + modelC.getCompleted()

                _itemsData.postValue(ActionState.success(allData))
            }
        }
    }

    fun clearDb() {
        viewModelScope.launch(exceptionHandler) {
            withContext(Dispatchers.IO) {
                cheggRepository.clearDB()
            }
        }

    }

    private val exceptionHandler = CoroutineExceptionHandler { _, exception ->
        exception.printStackTrace()
        _itemsData.postValue(ActionState.error(exception.message.toString()))
    }

    fun updateRequestDelay(){
        requestDelay = if (isAnimationOn()) Consts.NO_ANIMATION_DELAY else Consts.TWO_SECONDS_ANIMATION_DELAY
    }

    fun isAnimationOn() : Boolean{
        return requestDelay == Consts.TWO_SECONDS_ANIMATION_DELAY
    }
}