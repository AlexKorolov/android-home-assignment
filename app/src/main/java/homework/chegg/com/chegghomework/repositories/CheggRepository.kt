package homework.chegg.com.chegghomework.repositories

import homework.chegg.com.chegghomework.db.AppDatabase
import homework.chegg.com.chegghomework.model.BaseModel
import homework.chegg.com.chegghomework.model.CheggItem
import homework.chegg.com.chegghomework.model.modelc.ModelC
import homework.chegg.com.chegghomework.model.modelc.ModelCItem
import homework.chegg.com.chegghomework.network.CheggApi

class CheggRepository(private val cheggApi: CheggApi, private val appDatabase: AppDatabase) {

    suspend fun getDataSourceA(): List<CheggItem> {
        var model = appDatabase.modelsDao().getModelA()
        if (isModelValid(model)) {
            model = cheggApi.getDataFromSourceA().body()!!
            setExpirationAndSave(model)
        }
        return model.prepareCheggItemList()
    }

    suspend fun getDataSourceB(): List<CheggItem> {
        var model  = appDatabase.modelsDao().getModelB()
        if (isModelValid(model)) {
            model = cheggApi.getDataFromSourceB().body()!!
            setExpirationAndSave(model)
        }
        return model.prepareCheggItemList()
    }

    suspend fun getDataSourceC(): List<CheggItem> {
        var model = appDatabase.modelsDao().getModelC()
        if (isModelValid(model)) {
            val response = cheggApi.getDataFromSourceC().body()
            model = ModelC(response as ArrayList<ModelCItem>)
            setExpirationAndSave(model)
        }
        return model.prepareCheggItemList()
    }

    private fun isModelValid(model: BaseModel): Boolean {
        if (model == null || model.isExpired()) {
            return true
        }
        return false
    }

    private fun setExpirationAndSave(model: BaseModel) {
        model.updateExpirationTime()
        appDatabase.modelsDao().insertModel(model)
    }

    fun clearDB() {
        appDatabase.modelsDao().clearAll()
    }
}