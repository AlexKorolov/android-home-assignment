package homework.chegg.com.chegghomework.network

import homework.chegg.com.chegghomework.common.Consts
import homework.chegg.com.chegghomework.model.modela.ModelA
import homework.chegg.com.chegghomework.model.modelb.ModelB
import homework.chegg.com.chegghomework.model.modelc.ModelCItem
import retrofit2.Response
import retrofit2.http.GET

interface CheggApi {

    @GET(Consts.DATA_SOURCE_A_URL)
    suspend fun getDataFromSourceA(): Response<ModelA>

    @GET(Consts.DATA_SOURCE_B_URL)
    suspend fun getDataFromSourceB(): Response<ModelB>

    @GET(Consts.DATA_SOURCE_C_URL)
    suspend fun getDataFromSourceC(): Response<List<ModelCItem>>

}