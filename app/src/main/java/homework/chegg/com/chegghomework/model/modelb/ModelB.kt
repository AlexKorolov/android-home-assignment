package homework.chegg.com.chegghomework.model.modelb

import androidx.room.Entity
import homework.chegg.com.chegghomework.common.extensions.toCheggItem
import homework.chegg.com.chegghomework.model.BaseModel
import homework.chegg.com.chegghomework.model.CheggItem
import homework.chegg.com.chegghomework.model.CheggItemConverter

@Entity
data class ModelB(
    val metadata: Metadata
) : BaseModel() {
    override fun prepareCheggItemList(): List<CheggItem> {
        return metadata.innerdata.map { data -> data.toCheggItem() }
    }

    override var CACHE_TIME: Long
        get() = 30 * 60 * 1000
        set(value) {}
}