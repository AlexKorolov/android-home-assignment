package homework.chegg.com.chegghomework.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import homework.chegg.com.chegghomework.model.CheggItem
import homework.chegg.com.chegghomework.model.modela.ModelA
import homework.chegg.com.chegghomework.model.modelb.ModelB
import homework.chegg.com.chegghomework.model.modelc.ModelC


@Database(
    entities = [CheggItem::class, ModelA::class, ModelB::class, ModelC::class],
    version = AppDatabase.DB_VERSION,
    exportSchema = false
)
@TypeConverters(DBConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun modelsDao(): ModelsDao

    companion object {
        const val DB_VERSION = 1
        const val DB_NAME = "chegg_db"
    }
}