package homework.chegg.com.chegghomework.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CheggItem(
    @PrimaryKey()
    val imageUrl: String,
    val subtitle: String,
    val title: String
)