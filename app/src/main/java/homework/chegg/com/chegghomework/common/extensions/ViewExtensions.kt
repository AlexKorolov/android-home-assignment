package homework.chegg.com.chegghomework.common.extensions

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import homework.chegg.com.chegghomework.R

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun ImageView.loadImage(url: String) =
    Glide.with(this)
        .load(url)
        .error(R.drawable.ic_baseline_error_24)
        .placeholder(R.drawable.placeholder)
        .into(this)