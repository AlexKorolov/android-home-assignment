package homework.chegg.com.chegghomework.common

import homework.chegg.com.chegghomework.model.CheggItem

typealias CheggItemsState = ActionState<List<CheggItem>>