package homework.chegg.com.chegghomework.model

interface CheggItemConverter {
    fun prepareCheggItemList(): List<CheggItem>
}