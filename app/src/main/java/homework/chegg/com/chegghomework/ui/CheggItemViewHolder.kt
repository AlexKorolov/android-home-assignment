package homework.chegg.com.chegghomework.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import homework.chegg.com.chegghomework.R
import homework.chegg.com.chegghomework.common.extensions.loadImage
import homework.chegg.com.chegghomework.model.CheggItem

class CheggItemViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.card_item, parent, false)) {

    private var image: ImageView? = null
    private var title: TextView? = null
    private var subtitle: TextView? = null

    init {
        image = itemView.findViewById(R.id.imageView_card_item)
        title = itemView.findViewById(R.id.textView_card_item_title)
        subtitle = itemView.findViewById(R.id.textView_card_item_subtitle)
    }

    fun bind(cheggItem: CheggItem) {
        image?.loadImage(cheggItem.imageUrl)
        title?.text = cheggItem.title
        subtitle?.text = cheggItem.subtitle
    }
}