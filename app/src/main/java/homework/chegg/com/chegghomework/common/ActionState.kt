package homework.chegg.com.chegghomework.common

/**
 * State Management for UI & Data
 */
sealed class ActionState<T> {

    /* Informs that action is started */
    class Loading<T> : ActionState<T>()

    /* Informs that action is finished with data */
    data class Success<T>(val data: T) : ActionState<T>()

    /* Informs that action is finished with error */
    data class Error<T>(val message: String) : ActionState<T>()

    companion object {

        fun <T> loading() = Loading<T>()

        fun <T> success(data: T) = Success(data)

        fun <T> error(message: String) = Error<T>(message)
    }

}