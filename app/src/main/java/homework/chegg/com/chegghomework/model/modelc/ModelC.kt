package homework.chegg.com.chegghomework.model.modelc

import androidx.room.Entity
import homework.chegg.com.chegghomework.common.extensions.toCheggItem
import homework.chegg.com.chegghomework.model.BaseModel
import homework.chegg.com.chegghomework.model.CheggItem
import homework.chegg.com.chegghomework.model.CheggItemConverter

@Entity
class ModelC(val dataItems: ArrayList<ModelCItem>) : BaseModel() {

    override fun prepareCheggItemList(): List<CheggItem> {
        return dataItems.map { data -> data.toCheggItem() }
    }

    override var CACHE_TIME: Long
        get() = 60 * 60 * 1000
        set(value) {}
}