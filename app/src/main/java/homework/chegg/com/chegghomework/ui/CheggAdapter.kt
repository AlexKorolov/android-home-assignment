package homework.chegg.com.chegghomework.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import homework.chegg.com.chegghomework.model.CheggItem

class CheggAdapter(var data: List<CheggItem>) :
    RecyclerView.Adapter<CheggItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheggItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CheggItemViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: CheggItemViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    fun updateData(updatedData: List<CheggItem>) {
        data = updatedData
        notifyDataSetChanged()
    }
}