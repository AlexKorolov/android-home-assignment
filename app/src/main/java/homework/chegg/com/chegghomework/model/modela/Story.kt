package homework.chegg.com.chegghomework.model.modela

data class Story(
    val imageUrl: String,
    val subtitle: String,
    val title: String
)