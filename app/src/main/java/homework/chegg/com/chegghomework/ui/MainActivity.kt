package homework.chegg.com.chegghomework.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.forEachIndexed
import androidx.recyclerview.widget.LinearLayoutManager
import homework.chegg.com.chegghomework.R
import homework.chegg.com.chegghomework.common.ActionState
import homework.chegg.com.chegghomework.common.extensions.hide
import homework.chegg.com.chegghomework.common.extensions.show
import homework.chegg.com.chegghomework.model.CheggItem
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.error_layout.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupToolbar()
        setupRecycler()
        initObserver()
        onRefreshData()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar as Toolbar)
    }

    private fun setupRecycler() {
        val adapter = CheggAdapter(emptyList())
        dataRecycler.layoutManager = LinearLayoutManager(this)
        dataRecycler.adapter = adapter
    }

    private fun initObserver() {
        mainViewModel.itemsData.observe(this, {
            when (it) {
                is ActionState.Loading -> {
                    initLoadingIndicator(true)
                }
                is ActionState.Success -> {
                    handleNewData(it.data)
                }
                is ActionState.Error -> {
                    handleError(it.message)
                }
            }
        })
    }

    private fun initLoadingIndicator(isLoading: Boolean) {
        if (isLoading) {
            dataRecycler.hide()
            errorLayout.hide()
            loadingIndicator.show()
        } else {
            loadingIndicator.hide()
        }
    }

    private fun handleNewData(data: List<CheggItem>) {
        dataRecycler.show()
        dataRecycler.apply {
            layoutAnimation =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down)
            scheduleLayoutAnimation()
            (adapter as CheggAdapter).updateData(data)
        }

        initLoadingIndicator(false)


    }

    private fun handleError(errorMsg: String) {
        errorLayout.show()
        error_message.text = errorMsg
        initLoadingIndicator(false)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main_activity, menu)
        updateAnimationIcon(menu)
        return true
    }

    private fun updateAnimationIcon(menu: Menu){
        if (mainViewModel.isAnimationOn()) {
            menu.getItem(2).icon = ContextCompat.getDrawable(this, R.drawable.ic_time_on)
        }
        else{
            menu.getItem(2).icon = ContextCompat.getDrawable(this, R.drawable.ic_time_off)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> {
                onRefreshData()
                return true
            }

            R.id.action_delete -> {
                mainViewModel.clearDb()
                return true
            }

            else -> {
                mainViewModel.updateRequestDelay()
                invalidateOptionsMenu()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onRefreshData() {
        mainViewModel.getAllSourceData()
    }
}